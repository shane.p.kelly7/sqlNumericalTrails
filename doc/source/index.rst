.. pyNumericalTrials documentation master file, created by
   sphinx-quickstart on Fri Jun 18 17:07:58 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyNumericalTrials's documentation!
=============================================

This library was created to provide automation tools to manage the production, analysis and reproduction of results for numerical experiments.
To do this, the library works with the following assumptions and definitions:
  1. There is a python script that defines a numerical *experiment*
  2. The experiment script takes a set of *arguments* defined via the ArgParse library.
  3. Each time the experiment script is run a *trial* of the experiment is preformed.
  4. The outcome (data produced) of a trial depends only on the arguments provided (and perhaps a system dependent random sampler).

It is recommended to avoid making a change to the experiment scripts that would prevent the repetition of a previous trial.
If such a change is need, we suggest you define a new experiment script.

Under these assumptions, the library provides tools that automate tracking the arguments run for each trial, and saving the results of such a trial in an identifiable location.

Here is a quick example that makes use of :py:mod:`pyNumericalTrials.trialManager`. Suppose you have some script with a few arguments:

.. code-block:: python 

    import argparse

    parser = argparse.ArgumentParser(description='Test Spin Boson Evolution Code')
    parser.add_argument('--nBosons', type=int, default=3 ,help='number of bosons')
    parser.add_argument('--alpha', type=float, default=1 ,help='coupling paramter')

    def compute_point(args, dataLoc):
         # do some computation based on args
         result = args.nBosons + args.alpha
         # save data
         np.savetxt(dataLoc+"results.txt", result)

    if __name__ == '__main__':
        args = parser.parse_args()
        compute_point(args, "/somelocation/")

You can then add the simple modification to track the parameters of the script in a sql Database

.. code-block:: python 

    import pyNumericalTrials.trialManager as tm

    experiementName = "01a_test"
    databaseDir = "./"
    trialInitser = tm.trialManager(parser, experiementName, "discription", databaseDir, addMaster = True)

    if __name__ == '__main__':
        dataLoc, args = trialInitser.parseNadd()

        compute_point(args, dataLoc)

        trialInitser.finalize()

In the database directory, a file called an expieremnts.db is created (if it does not exist) which contains a sqlite database.
This file can be read with a sqlite database viewer (see http://sqlitebrowser.org or vscode's sqlite addon), and contains a master table and a table for each experiment script called with a different "experiementName".
The master table lists every trial preformed, giving its experiment (experiementName), date, whether it completed or not, and possibly a description.
The experiment tables (with name experiementName) have for columns: each parameter defined by the argParse parser, the time taken for computation and a description column.
trialInitser will also create a directory for each experiment, and in that directory it will create a directory for each trial that the result of the experiment can store data in.
The location of that directory is given by dataLoc returned by trialInitser.parseNadd().
This allows the script to save data in a location unique to the time it was called.

The other purpose of this library is to manage execution of multiple trials with different parameters, random sampleing, parallelization, and data management.  These features are provided by :py:mod:`pyNumericalTrials.executionManager` and :py:mod:`pyNumericalTrials.readNplot`. A few examples are:

  1. Saving (:func:`averageNSave<pyNumericalTrials.executionManager.executionManager.averageNSave>`) and reading(:func:`get_results<pyNumericalTrials.readNplot.get_results>`) data into or from an sql database. 
  2. Sweeping a parameter of the experiment script. :py:mod:`pyNumericalTrials.executionManager`
  3. Averaging over random dictionaries.   (:func:`averageNSave<pyNumericalTrials.executionManager.executionManager.averageNSave>`)
  4. Automate parallelization of 2. and 3. :class:`executionManager<pyNumericalTrials.executionManager.executionManager>`

.. toctree::
   :maxdepth: 2

   trialManager
   executionManager
   readNplot




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
