.. pyNumericalTrials documentation master file, created by
   sphinx-quickstart on Fri Jun 18 17:07:58 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Module trialManager
=======================

This module defines a class used to track the parameters used for a given trial of an expierment.
An example use of the class is as follows:

.. code-block:: python 

    import pyNumericalTrials.trialManager as tm
    import argparse

    parser = argparse.ArgumentParser(description='Test Spin Boson Evolution Code')
    parser.add_argument('--nBosons', type=int, default=3 ,help='number of bosons')
    parser.add_argument('--alpha', type=float, default=1 ,help='coupling paramter')

    experiementName = "01a_test"
    databaseDir = "./"
    trialInitser = tm.trialManager(parser, experiementName, "discription", databaseDir, addMaster = True)

    def compute_point(args, dataLoc):
         # do some computation based on args
         result = args.nBosons + args.alpha
         # save data
         np.savetxt(dataLoc+"results.txt", result)

    if __name__ == '__main__':
        dataLoc, args = trialInitser.parseNadd()

        compute_point(args, dataLoc)

        trialInitser.finalize()


Structure of Expeirments Database
---------------------------------

The database created and managed by trialManager can be used for multiple experiment scripts.
The database will then have the following tables:
 * One master table: every trial for every experiment is listed here.
 * Multiple <<experimentName>> tables for each experiment script using the database.
 * possibly Multiple <<experimentName>>sweeps tables for each experiment script which has a "sweepfile" argument used. See :py:mod:`executionManager<pyNumericalTrials.executionManager>` for details on sweeping expierment script arguments.
Each script must have a different experimentName.
The master table has the following columns:
 * timestamp: date and time script was called
 * description: any given string
 * experimentName
 * state: either Initialized or Valid depending on if finalize was called before the experiment script exited.
 
The <<experimentName>> tables contain the following columns:
 * masterID: rowID of trial in master table
 * computationTime: time it took between parseNadd and finalize being called. Set to -1 before finalize is called.
 * 1 column for each argument of the experiment script, the arguments being swept will have NULL values

 The <<experiementName>>sweeps table has the following columns:
  * localID: rowID in <<experimentName>> table for the trial 
  * sweptArg: one the argument being swept
  * linOlog: linear or log sweeping
  * x0, xf, N: initial and finial value of sweep and number of steps in sweep
Multiple rows will appear for a given trial if that trial had multiple arguments swept.


Module members 
--------------

.. automodule:: pyNumericalTrials.trialManager
    :members:
