.. pyNumericalTrials documentation master file, created by
   sphinx-quickstart on Fri Jun 18 17:07:58 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Module readNplot
=======================
This module is used to read and plot data from the sql database contaning results.
Each result database is a finite representation of the function "compute_point" defining the numerical expierement:

compute_point: (arguments) -> (results)

After an experiment has been performed, the fixed parameters are stored in the Experiments Database, the swept parameters are stored in the Results Database, along with the results which is a dictionary with nD-arrays for values.  Therefore, the results' database implements the compute_point function with the fixed parameters partially applied:

result database: (location of database, swept arguments) -> (results)

The result database does this, by assigning a unique "arg_id" to each unique combination of swept arguements in the database.
Then each table storying result database row refers to this arg_id.
Thus the database implements:

result database: (location of database, swept arguments) -> arg_id -> (results)

There are 3 types of tasks that this module

1. Functions for reading data in to a standard format.
2. A few standard plotting functions that also return the data they plot. 
3. Functions for managing the swept arguments. For now these aren't ready for use. 

Currently, this module is a bit of a mess, but there a few clean functions.
The most important is :func:`pyNumericalTrials.readNplot.get_results` which give you the disordered average dictonary that the function compute_point(args, datadir) produced.

Reading data
------------
.. autofunction:: pyNumericalTrials.readNplot.get_results
.. autofunction:: pyNumericalTrials.readNplot.tracesFrom1DParamCut
.. autofunction:: pyNumericalTrials.readNplot.getSweepInfo
.. autofunction:: pyNumericalTrials.readNplot.get_result
.. autofunction:: pyNumericalTrials.readNplot.restructure2Dparams
.. autofunction:: pyNumericalTrials.readNplot.restructure3Dparams

Plotting
--------
.. autofunction:: pyNumericalTrials.readNplot.phaseDiagram
.. autofunction:: pyNumericalTrials.readNplot.traceLegendParams
.. autofunction:: pyNumericalTrials.readNplot.timecut

