.. pyNumericalTrials documentation master file, created by
   sphinx-quickstart on Fri Jun 18 17:07:58 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

Module executionManager
=======================
This module provides tools to manage the following:
 1) sweeping arguments of expierment scripts through a range of values,
 2) preforming random sampeling and averaging,
 3) parallelization of 1) and 2), 
 4) saving results to a file.

Managing Execution
------------------

The first three goals are accomplished by first adding three arguments to experiment script:

.. code-block:: python 

   parser.add_argument('--sweepfile', default='sweepfile.csv', help = 'file describing which parameters to sweep')
   parser.add_argument('--nDisorders', type = int, default=100, help = 'Number of random samples. Set to one to avoid disorder averaging')
   parser.add_argument('--CPUs', type = int, default=4, help = 'number of CPUs to USE')

and then using an executionManager object to execute a function of the form compute_point(args, datadir):

.. code-block:: python 

   if __name__ == '__main__':
      dataLoc, args = trialInitser.parseNadd()

      dnsCTRL = em.executionManager(args, dataLoc, compute_point)
      print("Executing...")
      dnsCTRL.executeParallel()

      trialInitser.finalize()

Here :func:`executeParallel<pyNumericalTrials.executionManager.executionManager.executeParallel>`, prepares a sequence of args based on what is specified in the sweepfile and in the number of disorders, and executes them in parallel using args.CPUs number of processors. compute_point can then save the result in the datadir for each computation done on the various arguments and disorders. The sweep file specifies how arguments are swept by

.. code-block::

   lin, example3,   0, 10,  4
   log, example2,   -1, 1,  4

where each line specifies which argument is swept, if the sweep is linear ('lin') or logrithmic ('log'), the start value, the end value, and the number of steps in the sweep.

Saving and Averaging Data
-------------------------

Having compute_point save the data results is often tedious as it later requires disorder averaging.  Therefore, this module also provides functionality to disorder average and save the data in a sql database.  The :py:mod:`readNplot<pyNumericalTrials.readNplot>` module then provides tools for reading and plotting data from the sql database. To use this feature, the user defined function compute_point(args, datadir) must return a dictionary in a form specified by a "result_format" dictonary which has the same keys as the results dictonary, but the values specify the dimension of the data

.. code-block:: python 

   # integer is dimention of data: currently only up to 2D
   results_format = {'mean': 0, 'var': 0, 'trace': 1, 'twoDdata': 2} 

   def compute_point(args, dataLoc):
      # do computation
      x = np.random.rand(1)[0]*args.example3
      # return "results dictionary"
      return {'mean':x,
               'var':x*x,
               'trace': np.linspace(0,10*x,5),
               'twoDdata': np.outer(range(10),range(10))
            }

Both the results_format variable and compute_point function are passed to the constructor for the :class:`executionManager<pyNumericalTrials.executionManager.executionManager>` object, and the :func:`averageNSave<pyNumericalTrials.executionManager.executionManager.averageNSave>` function is called in addition to the :func:`executeParallel<pyNumericalTrials.executionManager.executionManager.executeParallel>` function:

.. code-block:: python 

   if __name__ == '__main__':
      dataLoc, args = trialInitser.parseNadd()

      dnsCTRL = em.executionManager(args, dataLoc, compute_point, results_format)
      print("Executing...")
      dnsCTRL.executeParallel()
      print("Saving...")
      dnsCTRL.averageNSave(sqlSave = True)

      trialInitser.finalize()

A complete example is given in the Examples/ExecutionNDataExamples folder.

executionManager Class
----------------------
.. autoclass:: pyNumericalTrials.executionManager.executionManager
.. autofunction:: pyNumericalTrials.executionManager.executionManager.executeParallel
.. autofunction:: pyNumericalTrials.executionManager.executionManager.averageNSave

Dictionary and Averaging Tools
------------------------------
.. autofunction:: pyNumericalTrials.executionManager.dictionaryAvg
.. autofunction:: pyNumericalTrials.executionManager.avgKey
.. autofunction:: pyNumericalTrials.executionManager.dicToCSV

Internals
---------
.. autofunction:: pyNumericalTrials.executionManager.stripSeedNcombine
.. autofunction:: pyNumericalTrials.executionManager.averageSeedLabeledList
.. autofunction:: pyNumericalTrials.executionManager.prep_argSweepCombinations
.. autofunction:: pyNumericalTrials.executionManager.readSweepFile
.. autofunction:: pyNumericalTrials.executionManager.kernel

