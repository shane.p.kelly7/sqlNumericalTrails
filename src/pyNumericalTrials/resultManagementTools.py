from sqlalchemy import Table, Column, Integer, MetaData, Float, create_engine
import numpy as np

result_keys_format_example = {"Rexample0D": 0, "Rexample1D": 1, "Rexample2D": 2}
swept_args_format_example = {"arg1": 'float', "arg2": 'int'}

#### PREPARE TABLES ####
def writeTabels(result_nsArg_data, swept_args_format, result_keys_format, dirBase):
    """
    writes data to a database results.db in dirBase
    requires:

    # data to go into rows of tables
    result_data: a list of dictionaries with both swept_arg vlaue and keys and result data value and keys

    parameters which determine structure of tables
    swept_args_format: example given above
    result_keys_format: example given above
    """
    metadata, sweptArguments, phaseDiagram, traces1D, traces2D = prepTabels(swept_args_format, result_keys_format) 
    engine = create_engine('sqlite:///'+dirBase+'/results.db', echo=False)
    metadata.create_all(engine)
    conn = engine.connect()

    print('writing results')
    args_ids = write_all_sweptArguments(result_nsArg_data, swept_args_format, sweptArguments, conn)


    if 0 in result_keys_format.values():
        keys0D = [key for key in list(result_keys_format.keys()) if result_keys_format[key]==0]
        write_all_phaseDiagram(result_nsArg_data, args_ids, keys0D, phaseDiagram, conn)

    if 1 in result_keys_format.values():
        keys1D = [key for key in list(result_keys_format.keys()) if result_keys_format[key]==1]
        write_all_traces1D(result_nsArg_data, args_ids, keys1D, traces1D, conn)

    if 2 in result_keys_format.values():
        keys2D = [key for key in list(result_keys_format.keys()) if result_keys_format[key]==2]
        write_all_traces2D(result_nsArg_data, args_ids, keys2D, traces2D, conn)
    print('done writing results')

def prepTabels(swept_args_format, result_keys_format):
    metadata = MetaData()

    argColumns = []
    print(result_keys_format.values())
    for swept_arg in list(swept_args_format.keys()):
        if swept_args_format[swept_arg] == 'float':
            argColumns.append(Column(swept_arg, Float))
        elif swept_args_format[swept_arg] == 'int':
            argColumns.append(Column(swept_arg, Integer))
        else:
            print(swept_arg)
            raise("swept arg format not (float, int)")
    sweptArguments = Table('sweptArguments', metadata,
        Column('id', Integer, primary_key=True),
        *argColumns
    )

    if 0 in result_keys_format.values():
        keys0D = [key for key in list(result_keys_format.keys()) if result_keys_format[key]==0]
        argColumns = [Column(key, Float) for key in keys0D]
        phaseDiagram = Table('phaseDiagram', metadata,
            Column('id', Integer, primary_key=True),
            Column('args_id', Integer),
            *argColumns
         )
    else:
        phaseDiagram = None

    if 1 in result_keys_format.values():
        keys1D = [key for key in list(result_keys_format.keys()) if result_keys_format[key]==1]
        argColumns = [Column(key, Float) for key in keys1D]
        traces1D = Table('traces1D', metadata,
            Column('id', Integer, primary_key=True),
            Column('args_id', Integer),
            Column('trace_step', Integer),
            *argColumns
         )
    else:
        traces1D = None

    if 2 in result_keys_format.values():
        keys2D = [key for key in list(result_keys_format.keys()) if result_keys_format[key]==2]
        argColumns = [Column(key, Float) for key in keys2D]
        traces2D = Table('traces2D', metadata,
            Column('id', Integer, primary_key=True),
            Column('args_id', Integer),
            Column('x_step', Integer),
            Column('t_step', Integer),
            *argColumns
         )
    else:
        traces2D = None

    return metadata, sweptArguments, phaseDiagram, traces1D, traces2D

#### WRITE TABLES ####
def write_all_sweptArguments(result_n_sArg_data, swept_args_format, sweptArguments, conn):
    rows = []
    for n, result in enumerate(result_n_sArg_data):
        row = {key: result[key] for key in swept_args_format}
        row['id'] = n+1
        rows.append(row)
    conn.execute(sweptArguments.insert(), rows)
    args_ids = np.int_(np.array(range(len(result_n_sArg_data)))+1)
    return args_ids

def write_all_phaseDiagram(results, args_ids, keys0D, phaseDiagram, conn):
    rows = []
    for result, args_id in zip(results, args_ids):
        row = {key: result[key] for key in keys0D}
        row['args_id'] = int(args_id)
        rows.append(row)
    conn.execute(phaseDiagram.insert(), rows)
    return

def write_sweptArguments(result, swept_args_format, sweptArguments, conn):
    row = [{key: result[key] for key in swept_args_format}]
    rr = conn.execute(sweptArguments.insert(), row)
    return rr.inserted_primary_key[0]

def write_phaseDiagram(result, args_id, keys0D, phaseDiagram, conn):
    row = {key: result[key] for key in keys0D}
    row['args_id'] = args_id
    conn.execute(phaseDiagram.insert(), [row])
    return

def write_all_traces1D(result_data, args_ids, keys1D, traces1D, conn):
    """
    given:
    a list of dictionary of results (results)
    a list of args table keys (args_ids)
    the list of 1D keys(keys1D)
    the sql table (traces1D) and connection (conn)

    this function writes the results to a table with format:

    result_key1, result_key2, ..., args_id, trace_step
    """
    rows = []
    for args_id, result in zip(args_ids, result_data):
        traceSizes = [len(result[key]) for key in keys1D]
        maxSizeOfTrace = max(traceSizes) 

        for trace_step in range(maxSizeOfTrace):
            row = {} #sql ro
            for key in keys1D:
                if trace_step<len(result[key]):
                    row[key] = result[key][trace_step]
                else:
                    row[key] = None
            row['args_id'] = int(args_id)
            row['trace_step'] = trace_step
            rows.append(row)
    conn.execute(traces1D.insert(), rows)

def write_all_traces2D(result_data, args_ids, keys2D, traces2D, conn):
    """
    given:
    a list of dictionary of results (results)
    a list of args table keys (args_ids)
    the list of 2D keys(keys2D)
    the sql table (traces2D) and connection (conn)

    this function writes the results to a table with format:

    result_key1, result_key2, ..., args_id, x_step, t_step
    """
    rows = []
    for args_id, result in zip(args_ids, result_data):
        traceShapes = [np.shape(result[key]) for key in keys2D]
        tlength, xlength = maxTuple(traceShapes) 

        for t_step in range(tlength):
            for x_step in range(xlength):
                row = {} #sql ro
                for key in keys2D:
                    if t_step<len(result[key]):
                        if x_step<len(result[key][t_step]):
                            row[key] = result[key][t_step][x_step]
                    else:
                        row[key] = None
                row['args_id'] = int(args_id)
                row['x_step'] = x_step
                row['t_step'] = t_step
                rows.append(row)
    conn.execute(traces2D.insert(), rows)

def maxTuple(listOtuples):
    x0max = listOtuples[0][0]
    x1max = listOtuples[0][1]
    for tup in listOtuples:
        if x0max<tup[0]:
            x0max=tup[0]
        if x1max<tup[1]:
            x1max=tup[1]
    return (x0max, x1max)

def write_traces1D(result, args_id, keys1D, traces1D, conn):
    rows = []
    exampleTrace = result[keys1D[0]]
    sizeOfTraces = len(exampleTrace)
    for trace_step in range(sizeOfTraces):
        row = {key: result[key][trace_step] for key in keys1D}
        row['args_id'] = args_id
        row['trace_step'] = trace_step
        rows.append(row)
    conn.execute(traces1D.insert(), rows)

from sqlalchemy.sql import select
#### READ TABLES ####
def get_engineNconn(dirBase):
    engine = create_engine('sqlite:///'+dirBase+'/results.db')
    conn =engine.connect()
    return (engine, conn)

def read_sweptArguments(dirBase):
    """
    Args:
        dirBase: database directory with results.db
    Returns:
      sweptArgArray: sweptArgument table in array form
      uniqueargValues: dictonary between name of swept parameter and the unique values taken by that parameter in the table
      valueToIdDict: a dictionary which, from a list of argument values, returns the row id, the list of args must have the same order as in sweptParamNames
      sweptParamNames: names of parameters swept, the order of this list determines the order in which the arg values must appear in the key to the valueToIdDict
    """

    # load table
    engine = create_engine('sqlite:///'+dirBase+'/results.db')
    conn =engine.connect()
    metadata = MetaData()
    metadata.reflect(engine)
    sweptArguments = metadata.tables['sweptArguments']

    sweptParamNames = sweptArguments.columns.keys()
    sweptParamNames.pop(sweptParamNames.index('id'))

    s = select([sweptArguments])
    sweptArgTable = conn.execute(s)

    sweptArgArray = []

    # prepare valueToIdDict and sweptArgArray
    valueToIdDict = {}
    for row in sweptArgTable:
        sweptParamRow = [row[key] for key in sweptParamNames]

        valueToIdDict[tuple(sweptParamRow)] = row['id']
        sweptParamRow.append(float(row['id']))
        sweptArgArray.append(sweptParamRow)

    # Prepare uniqueArgValues
    uniqueArgValues = {}
    sweptArgArray = np.array(sweptArgArray)
    for n, argName in enumerate(sweptParamNames):
        up = list(set(sweptArgArray[:,n]))
        up.sort
        uniqueArgValues[argName] = np.array(up)

    return sweptArgArray, uniqueArgValues, valueToIdDict, sweptParamNames

def read_phaseDiagram(engine, conn, arg_id):
    metadata = MetaData()
    metadata.reflect(engine)
    phaseDiagram = metadata.tables['phaseDiagram']
    # s = students.select().where(students.c.id>2)
    s = phaseDiagram.select().where(phaseDiagram.c.args_id==arg_id)
    sqlresult = conn.execute(s)
    return [dict(r) for r in sqlresult]

def read_trace1D(engine, conn, arg_id):
    """
    given a arg_id (row id for swept argument table identifying the arguements) reads from the trace1D table and returns a 
    table with results only from that arg_id
    """
    metadata = MetaData()
    metadata.reflect(engine)
    traces1D = metadata.tables['traces1D']
    # s = students.select().where(students.c.id>2)
    s = traces1D.select().where(traces1D.c.args_id==arg_id).order_by(traces1D.c.trace_step)
    sqlresult = conn.execute(s)
    return [dict(r) for r in sqlresult]

def read_trace2D(engine, conn, arg_id, obs):
    """
    given a arg_id  and obserable label reads from the trace2D table and returns a 
    2D numpy array 
    """
    metadata = MetaData()
    metadata.reflect(engine)
    traces2D = metadata.tables['traces2D']
    # s = students.select().where(students.c.id>2)
    colms2select = [traces2D.c.t_step, traces2D.c.x_step, getattr(traces2D.c, obs)]
    s = select(colms2select).where(traces2D.c.args_id==arg_id).order_by(traces2D.c.t_step)
    sqlresult = conn.execute(s)
    return [dict(r) for r in sqlresult]

def read_trace1DFast(arg_id, conn, engine):
    """
    given a arg_id (row id for swept argument table identifying the arguements) reads from the trace1D table and returns a 
    table with results only from that arg_id
    """
    metadata = MetaData()
    metadata.reflect(engine)
    traces1D = metadata.tables['traces1D']
    # s = students.select().where(students.c.id>2)
    s = traces1D.select().where(traces1D.c.args_id==arg_id).order_by(traces1D.c.trace_step)
    subTrial1DtraceTable = conn.execute(s)
    return [dict(r) for r in subTrial1DtraceTable]

# process tables for observable
def extract1DtraceForObs(subTrial1DtraceTable, obs):
    return [ row[obs] for row in subTrial1DtraceTable]

def rowsTo2Darray(obsRows, obs):
    tmax = -1
    xmax = -1
    rowsArray = []
    for row in obsRows:
        t_step = row['t_step']
        x_step = row['x_step']
        obs_value = row[obs]
        if x_step>xmax:
            xmax=x_step
        if t_step>tmax:
            tmax=t_step
        rowsArray.append([t_step, x_step, obs_value])

    npa = np.zeros((tmax+1,xmax+1))
    for row in rowsArray:
        t_step, x_step, obs_value = row
        npa[t_step,x_step] = obs_value

    return npa

