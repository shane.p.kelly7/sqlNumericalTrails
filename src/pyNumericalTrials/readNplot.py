import pyNumericalTrials.resultManagementTools as rdt
from matplotlib import pyplot as plt
import numpy as np

# tools for preparing argument sweeps
def getSweepInfo(loc):
    """
    This function returns the swept parameters and their values for a database at loc

    Args:
        loc: location of result database
    
    Returns: (sweptParams, uniqueParamValues)
        * sweptParams: list of swept parameters
        * uniqueParamValues: dictionary of unique values for each parameter
    """
    sweptArgTable, uniqueParamValues, ToArgIdDict, sweptParams = rdt.read_sweptArguments(loc)
    return uniqueParamValues, sweptParams

def getSweptArgIdsFor1DCut(varParamValues, fixedIndicies, uniqueParamValues, sweptParams, ToArgIdDict, varParam):
    """
    gets arg_ids for data to be read.
    requires:
    varParamValues:  values taken by the parameter being varied in the legend
    fixedIndicies: indicies of fixed parameters
    uniqueParamValues: unique values taken by different parameters in the sweep 
    sweptParams
    ToArgIdDict
    """
    swept_arg_ids = []
    for varArg in varParamValues:
        # generate the unique tupple to identify the index given the arg specified by the fixed indicies
        argIdDict_key = []
        for n, col in enumerate(sweptParams):
            if col == varParam:
                argIdDict_key.append(varArg)
            else:
                argIdDict_key.append(uniqueParamValues[col][fixedIndicies[n]])
        arg_id =ToArgIdDict[tuple(argIdDict_key)]
        swept_arg_ids.append(arg_id)
    return swept_arg_ids

def prepare_param_space_1Dcut(loc, varParam, fixedIndicies=[], plotFrom=0, plotEvery=1, indicies=[]):
    """
    returns:
    swept_arg_ids: arg_ids for reading trace table
    varParamValues:  values taken by the parameter being varied
    fixedArgString: string describing the value of the other parameters
    """

    # read sweptArgument table
    sweptArgArray, uniqueParamValues, ToArgIdDict, sweptParams = rdt.read_sweptArguments(loc)

    # check if fixed indicies are given
    if len(fixedIndicies)==0:
        fixedIndicies = np.int_(np.zeros(len(sweptParams)))
    else:
        fixedIndicies = fixedIndicies

    # get arguments for the varParam
    if varParam == '':
        raise('need varParam')
    else:
        varParamValues = np.sort(uniqueParamValues[varParam])
    if len(indicies)==0:
        varParamValues = varParamValues[plotFrom::plotEvery]
    else:
        varParamValues = [varParamValues[i] for i in indicies]

    # string describing values kept constant in sweep
    fixedArgString = ""
    for n, col in enumerate(sweptParams):
        if not col == varParam:
            fixedArgString += str(col) + '='
            fixedArgString += str(uniqueParamValues[col][fixedIndicies[n]])
            fixedArgString += '|'

    swept_arg_ids = getSweptArgIdsFor1DCut(varParamValues, fixedIndicies, uniqueParamValues, sweptParams, ToArgIdDict, varParam)
    return swept_arg_ids, varParamValues, fixedArgString

def prepare_2D_param_space_cut(loc, varParamX, varParamY, fixedIndicies):
    sweptParamArray, uniqueParamValues, spdict, column_names = rdt.read_sweptArguments(loc)

    # check if fixed indicies are given
    if len(fixedIndicies)==0:
        fixedIndicies = np.int_(np.zeros(len(column_names)))
    else:
        fixedIndicies = fixedIndicies

    arg_ids = []
    argsOfParamX = np.sort(uniqueParamValues[varParamX])
    argsOfParamY = np.sort(uniqueParamValues[varParamY])
    # go through all the args in the sweep arg
    for varArgX in argsOfParamX:
        arg_idsVy = []
        for varArgY in argsOfParamY:
            uniqueParam = []
            # generate the unique tuple to identify the index given the args specified by the fixed indicies
            for n, col in enumerate(column_names):
                if col == varParamX:
                    uniqueParam.append(varArgX)
                elif col == varParamY:
                    uniqueParam.append(varArgY)
                else:
                    uniqueParam.append(uniqueParamValues[col][fixedIndicies[n]])
            arg_id =spdict[tuple(uniqueParam)]
            arg_idsVy.append(arg_id)
        arg_ids.append(arg_idsVy)
    fixedArgStrings = "NOT DEFINED"
    return arg_ids, argsOfParamX, argsOfParamY, fixedArgStrings

# tools for reading results
def get_result(loc, arg_id, results_format):
    """ Gets results for specific arg_id

    Args:
        loc: location of result database
        arg_id: row in sweptArguments table
        results_format: shape of those keys, must match what is stored in results database

    Reutrns:
        Dictionary with result_keys as keys and either a value,  a 1D array or 2D array depending on result_keys_format
    """
    (engine, conn) = rdt.get_engineNconn(loc)
    return get_resultF(engine, conn, arg_id, results_format)

def get_resultF(engine, conn, arg_id, results_format):
    """ Gets results for specific arg_id

    Args:
        engine: sqlAlchamy engine
        conn: sqlAlchamy connection
        arg_id: row in sweptArguments table
        results_format: shape of those keys, must match what is stored in results database

    Reutrns:
        Dictionary with result_keys as keys and either a value,  a 1D array or 2D array depending on result_keys_format
    """
    result = {}
    if 0 in results_format.values():
        keys0D = [key for key in list(results_format.keys()) if results_format[key]==0]
        results0D = rdt.read_phaseDiagram(engine, conn, arg_id)
        row = results0D[0]
        for obs in keys0D:
            result[obs]=row[obs]

    if 1 in results_format.values():
        keys1D = [key for key in list(results_format.keys()) if results_format[key]==1]
        subTrial1DtraceTable =  rdt.read_trace1D(engine, conn, arg_id)
        for obs in keys1D:
            trace = rdt.extract1DtraceForObs(subTrial1DtraceTable, obs)  
            result[obs]=trace

    if 2 in results_format.values():
        keys2D = [key for key in list(results_format.keys()) if results_format[key]==2]
        sql2Dtable = rdt.read_trace2D(engine, conn, arg_id, obs)
        for obs in keys2D:
            result[obs]= rdt.rowsTo2Darray(sql2Dtable,obs)
    return result

def get_results(loc, results_format):
    """ Returns results saved in results database at location loc

    Args:
        loc: location of results database
        results_format: shape of those keys, must match what is stored in results database

    Returns: (paramList, resultDictList)
        paramList: list of dictonaires that describe the current swept parameter
        resultDictList: list of result dictionaries with keys given by result_keys
    """
    sweptArgArray, uniqueArgValues, valueToIdDict, sweptParamNames = rdt.read_sweptArguments(loc)

    # prepare param list
    (engine, conn) = rdt.get_engineNconn(loc)
    paramList = [{name:value for (name, value) in zip(sweptParamNames,argList)} for argList in sweptArgArray]
    arg_ids = [valueToIdDict[tuple(key[:-1])] for key in sweptArgArray]
    resultDictList = [get_resultF(engine, conn, arg_id, results_format) for arg_id in arg_ids]
    return paramList, resultDictList

def tracesFrom1DParamCut(loc, varParam, plotObs, fixedIndicies=[], plotFrom=0, plotEvery=1, var_indicies=[]):
    """
    reads 1D results specified by plotObs 

    Args:
        loc: location of results database
        varParam: name of swept parameter indexing the results
        plotObs: name of results to read
        fixedIndicies: indicies of swept parameters that are kept constant
        var_indicies: list of indicies of parameters to vary in the legend if given overrides the following two parameters
        plotFrom: index of virst param being varied to plot
        plotEvery: indx = plotFrom + plotEvery*i
    
    Returns: (traces, varParamValues, fixedArgString)
        traces: A dictionary with keys being the observable, and value being a list of 1D array, the order of the list is the same as the varParamsValues
        varParamsValues: list of values taken by varParam indexing the list in the traces dictionary
        fixedArgString: string describing fixed parameters
    """
    # read sweep data:
    swept_arg_ids, varParamValues, fixedArgString = prepare_param_space_1Dcut(loc, varParam, fixedIndicies, plotFrom, plotEvery, var_indicies)
    traces = {}
    (engine, conn) = rdt.get_engineNconn(loc)
    for obs in plotObs:
        traces[obs]=[]
        for arg_id, var_arg in zip(swept_arg_ids, varParamValues):
            # read 1D trace from database
            subTrial1DtraceTable =  rdt.read_trace1D(engine, conn, arg_id)
            trace = rdt.extract1DtraceForObs(subTrial1DtraceTable, obs)  
            traces[obs].append(trace)
        traces[obs]=np.array(traces[obs])
    return traces, varParamValues, fixedArgString

# managing results and arguments after reading data from get_results
def restructure3Dparams(paramList, resultList, xkey, ykey, zkey):
    """
    This function takes the output of get_results and restuctures the results into a 3D array with the x, y and z dimention corresponding to the xkey, ykey and zkey respectively

    Args:
        paramList: list of dictionaries specifying arguments
        resultList: list of dictionaries of results
        xkey: key for x axis
        ykey: key for y axis
        zkey: key for z axis
    
    Retuns:
        reshaped: 3D array of results
    """
    xs = np.unique([param[xkey] for param in paramList])
    ys = np.unique([param[ykey] for param in paramList])
    zs = np.unique([param[zkey] for param in paramList])
    reshapeds = np.zeros(len(xs),len(ys),len(zs))
    for param, result in zip(paramList,resultList):
        xindx = np.where(xs==param[xkey])[0][0]
        yindx = np.where(ys==param[ykey])[0][0]
        zindx = np.where(zs==param[zkey])[0][0]
        #Zs[xindx,yindx]=result[result_key]

        reshapeds[xindx,yindx,zindx] = result
    return reshapeds

def restructure2Dparams(paramList, resultList, xkey, ykey):
    """
    This function takes the output of get_results and restuctures the results into a 2D array with the x and y dimention corresponding to the xkey and ykey respectively

    Args:
        paramList: list of dictionaries specifying arguments
        resultList: list of dictionaries of results
        xkey: key for x axis
        ykey: key for y axis
    
    Retuns:
        reshaped: 2D array of results
    """
    xs = np.unique([param[xkey] for param in paramList])
    ys = np.unique([param[ykey] for param in paramList])
    reshapeds = np.zeros(len(xs),len(ys))
    for param, result in zip(paramList,resultList):
        xindx = np.where(xs==param[xkey])[0][0]
        yindx = np.where(ys==param[ykey])[0][0]

        reshapeds[xindx,yindx] = result
    return reshapeds

# standard plotting functions
def phaseDiagram(loc, result_key, key_type=0,  label='', time=-1, average=False,fixed={}, cmap='viridis', log=False, yscale=1, ax=-1):
    """
    This function produces a 2D phase diagram color plot for a 0D result key, and a sweep that has exactly two swept parameters.

    Args:
        loc: location of results database
        result_key: result key for a 0D result
    
    Returns: (XS, YS, Zs)
        * XS: 2D array for X variables
        * YS: 2D array for Y variables
        * Zs: 2D array for 0D result varable
    """
    #get matplotlib axis
    if ax==-1:
        fig, ax = plt.subplots()
    result_keys_format = {result_key:key_type}
    paramlist, resultlist = get_results(loc,  result_keys_format)
    keys = list(paramlist[0].keys())
    print(key_type)
    print(key_type==1)

    def getPDresult(result,result_key,key_type, log=False):
        if key_type==0:
            r = result[result_key]
        elif key_type == 1:
            rlistin = result[result_key]
            rlist = [r for r in rlistin if r!=None]
            if average:
                r = np.average(rlist[time:])
            else:
                r = rlist[time]
        if log:
            return np.log(r/yscale)
        else:
            return r/yscale
    if len(keys)-len(fixed)>1:
        xkey = keys[0]
        ykey = keys[1]
        xs = np.unique([param[xkey] for param in paramlist])
        ys = np.unique([param[ykey] for param in paramlist])
        XS, YS = np.meshgrid(xs,ys)
        Zs = np.zeros(np.shape(XS)).T
        for param, result in zip(paramlist,resultlist):
            xindx = np.where(xs==param[xkey])[0][0]
            yindx = np.where(ys==param[ykey])[0][0]
            #Zs[xindx,yindx]=result[result_key]
            Zs[xindx,yindx] = getPDresult(result, result_key, key_type, log)
        c = ax.pcolor(XS, YS, Zs.T, shading='auto', cmap=cmap)
        plt.colorbar(c, ax=ax)
        ax.set_xlabel(xkey)
        ax.set_ylabel(ykey)
        return XS,YS,Zs
    else:
        if len(fixed)==1:
            ykey = list(fixed.keys())[0]
            keys.remove(ykey)
            xkey = keys[0]
            ys = np.unique([param[ykey] for param in paramlist])
            xs = np.unique([param[xkey] for param in paramlist])
            yfixedindx = (np.abs(ys-fixed[ykey])).argmin()
            print(yfixedindx)
        else:
            xkey = keys[0]
            xs = np.unique([param[xkey] for param in paramlist])
        Zs = np.zeros(np.shape(xs))
        for param, result in zip(paramlist,resultlist):
            xindx = np.where(xs==param[xkey])[0][0]
            if len(fixed)==1:
                yindx = np.where(ys==param[ykey])[0][0]
                if yindx==yfixedindx:
                    Zs[xindx] = getPDresult(result, result_key, key_type, log)
            else:
                Zs[xindx] = getPDresult(result, result_key, key_type, log)
        from matplotlib import pyplot as pyplot
        ax.plot(xs,Zs,label=label)
        ax.set_xlabel(xkey)
        ax.set_ylabel(result_key)
        return xs,Zs

def traceLegendParams(loc, varParam, plotObs, fixedIndicies=[], plotFrom=0, plotEvery=1, dt=1,log=False, logy=False, loglog=False, fit=False, saveName='NOPLOT', title='', xlabel='t', var_indicies=[], legend=True, plot=True):
    """
    This function produces a multiple plots for a 1D result, with different values of the varParam in the legend and different plots corresponding to different observalbes.

    Args:
        loc: location of results database
        varParam: parameter to vary in the legend
        plotObs: list of result key for a 1D result: ["key1", "key2"]
        fixedIndicies: list of indicies of parameters to fix
        dt: how to scale x axis of figure.  x = np.array(range(len(trace)))*dt
        log: whether to plot semilogx
        logy: whether to plot semilogy
        loglog: whether to plot loglog
        fit: whether to fit a curve to the data
        saveName: name of file to save figure to (default NOPLOT means it doesn't save a figure)
        title: title of figure
        xlabel: label for x axis
        var_indicies: list of indicies of parameters to vary in the legend if given overrides the following two parameters
        plotFrom: index of virst param being varied to plot
        plotEvery: indx = plotFrom + plotEvery*i
        legend: whether to plot a legend
        plot: whether to plot the figure or just return data
    
    Returns: (traces, varParamList)
        * traces: A dictionary with keys being the observable, and value being a list of 1D array, the order of the list is the same as the varParamsValues
        * varParamsValues: list of values taken by varParam in the legend/database
    """
    # read sweep data:
    traces, varParamValues, fixedArgString = tracesFrom1DParamCut(loc, varParam, plotObs, fixedIndicies, plotFrom, plotEvery, var_indicies)

    ###### Plotting #######
    # prepare colors
    sweep_size = len(varParamValues)

    # different plots for different observables
    for obs in plotObs:
        n=0
        if plot:
            plt.figure()
        for trace, var_arg in zip(traces[obs], varParamValues):
            # prepare x values
            t = np.array(range(len(trace)))
            t=t*dt
            x=np.array(range(len(trace)))

            # prepare legend label
            if sweep_size<8:
                labelname = np.round(var_arg,3)
            elif n==0 or n==sweep_size-1:
                labelname = varParam + ': ' + str(np.round(var_arg,3))
            else:
                labelname = ''

            # plot given a scale
            if plot:
                if log:
                    plt.semilogx(t, trace, label = labelname)
                elif logy:
                    plt.semilogy(t, trace, label = labelname)
                elif loglog:
                    plt.loglog(t, trace, label = labelname)
                else:
                    plt.plot(t, trace, label = labelname)

            # try fitting a straght line
            if fit:
                if log:
                    print("fit", np.polyfit(np.log(x[1000:]), trace[1000:],1))
                else:
                    print("fit", np.polyfit((x[1000:]), trace[1000:],1))
                n+=1
        if plot:
            if title =='':
                plt.title(fixedArgString)
            else:
                plt.title(title)
            plt.ylabel(obs)
            plt.xlabel(xlabel)
            if legend:
                plt.legend()
            if not (saveName == 'NOPLOT'):
                plt.savefig(saveName+obs+'.pdf')
    return traces, varParamValues

def timecut(loc, varParam, plotObs, time, average=False, fixedIndicies=[], plotFrom=0, plotEvery=1, dt=1,log=False, logy=False, loglog=False, fit=False, saveName='NOPLOT', title='', xlabel='t', var_indicies=[], plot=True):
    
    """
    This function plots time cuts or averages v.s. the varParam.
    It has the same functionality as traceLegendParams, but it plots time cuts instead of traces.
 
    Args:
        time: time to cut at or average after
        average: whether to average or not
    Returns: (cutvalues, varParamsvalues)
        cutvalues: A dictionary with keys being the observable, and value being a list of 1D array, the order of the list is the same as the varParamsValues
        varParamsValues: list of values taken by varParam in the x-axis of plot
    """

    # read sweep data:
    traces, varParamValues, fixedArgString = tracesFrom1DParamCut(loc, varParam, plotObs, fixedIndicies, plotFrom, plotEvery, var_indicies)

    ###### Plotting #######
    # prepare colors
    sweep_size = len(varParamValues)

    # differert plots for different observables
    cutvalues = {}
    for obs in plotObs:
        n=0
        plotyvalues = []
        for trace, var_arg in zip(traces[obs], varParamValues):
            # read 1D trace from database
            if average:
                plotyvalues.append(np.average(trace[time:]))
            else:
                plotyvalues.append(trace[time])
        cutvalues[obs]  = np.array(plotyvalues)


        if plot:
            plt.plot(varParamValues, plotyvalues)
            if title =='':
                plt.title(fixedArgString)
            else:
                plt.title(title)
            plt.ylabel(obs)
            plt.xlabel(varParam)
            if not (saveName == 'NOPLOT'):
                plt.savefig(saveName+obs+'.pdf')
    return cutvalues, varParamValues

# Old/ Not used
def magsqrdFrom1DParamCut(loc, varParam, vecObs, fixedIndicies=[], plotFrom=0, plotEvery=1, var_indicies=[], dt=1, plot=True, average=True, timecut =-1):
    traces, varParamValues, fixedArgString = tracesFrom1DParamCut(loc, varParam, vecObs, fixedIndicies, plotFrom, plotEvery, var_indicies)
    magsqrdtraces = traces[vecObs[0]]**2+traces[vecObs[1]]**2
    if timecut>0:
        result = []
        for trace, param in zip(magsqrdtraces, varParamValues):
            if average:
                result.append(np.average(trace[timecut:]))
            else:
                result.append(trace[timecut:])
    else:
        result = traces


    if plot:
        if timecut<0:
            for trace, param in zip(magsqrdtraces, varParamValues):
                t = np.array(range(len(trace)))
                t=t*dt
                plt.plot(t, trace, label=param)
            plt.legend()
            plt.title(fixedArgString)
        else:
            plt.plot(varParamValues, result, label = fixedArgString)
            plt.legend()
    return result, varParamValues, fixedArgString


def tracesFrom2DParamCut(loc, varParamX, varParamY, plotObs, fixedIndicies=[]):
    """
    returns:
    traces[ParamXIdx, ParamYIdx, timeIdx]
    argsOfParamX
    argsOfParamY
    fixedArgString
    """
    arg_ids, argsOfParamX, argsOfParamY, fixedArgStrings = prepare_2D_param_space_cut(loc, varParamX, varParamY, fixedIndicies)
    traces = {}
    (engine, conn) = rdt.get_engineNconn(loc)
    for obs in plotObs:
        traces[obs]=[]
        for arg_idsVy, varX_arg in zip(arg_ids, argsOfParamX):
            tracesVparamY = []
            for arg_id, varY_arg in zip(arg_idsVy, argsOfParamY):
                # read 1D trace from database
                subTrial1DtraceTable =  rdt.read_trace1D(engine, conn, arg_id)
                trace = rdt.extract1DtraceForObs(subTrial1DtraceTable, obs)  
                tracesVparamY.append(trace)
            traces[obs].append(tracesVparamY)
        traces[obs]=np.array(traces[obs])
    return traces, argsOfParamX, argsOfParamY, fixedArgStrings

def averagesFrom2DParamCut(loc, varParamX, varParamY, plotObs, time, average=True, fixedIndicies=[], plot=True):
    traces, argsOfParamX, argsOfParamY, fixedArgString = tracesFrom2DParamCut(loc, varParamX, varParamY, plotObs, fixedIndicies)
    averages ={}
    for obs in plotObs:
        averages[obs]=[]
        for tracesVparamY in traces[obs]:
            averagesVy = []
            for traces in tracesVparamY:
                if average:
                    averagesVy.append(np.average(traces[time:]))
                else:
                    averagesVy.append(np.average(traces[time]))
            averages[obs].append(averagesVy)
        averages[obs]=np.array(averages[obs])
    if plot:
        plotResultFrom2DParamCut(averages, argsOfParamX, argsOfParamY, plotObs, varParamX)
    return averages, argsOfParamX, argsOfParamY, fixedArgString

def plotResultFrom2DParamCut(values, argsOfParamX, argsOfParamY, plotObs, varParamX):
    for obs in plotObs:
        plt.figure()
        for argOfParamY, valuesVx in zip(argsOfParamY, np.transpose(values[obs])):
            plt.plot(argsOfParamX, valuesVx,label=argOfParamY)
        plt.xlabel(varParamX)
        plt.ylabel(obs)
        plt.legend()

def traceLegendObs(loc, varParam, plotObs, fixedIndicies=[], plotFrom=0, plotEvery=1, dt=1,log=False, logy=False, loglog=False, fit=False, saveName='NOPLOT', title='', xlabel='t', var_indicies=[]):
    ##### PREPRARE SWEPT DATA #######
    swept_arg_ids, varParamValues, fixedArgString = prepare_param_space_1Dcut(loc, varParam, fixedIndicies, plotFrom, plotEvery, var_indicies)

    (engine, conn) = rdt.get_engineNconn(loc)
    for arg_id, var_arg in zip(swept_arg_ids, varParamValues):
        plt.figure()
        for obs in plotObs:
            traceBobservables =  rdt.read_trace1D(engine, conn, arg_id)
            trace = [ row[obs] for row in traceBobservables]
            t = np.array(range(len(trace)))
            t=t*dt
            if log:
                plt.semilogx(t, trace, label = obs)
            elif loglog:
                plt.loglog(t, trace, label = obs)
            elif logy:
                plt.semilogy(t, trace, label = obs)
            else:
                plt.plot(t, trace, label = obs)
        plt.title(varParam + ': ' + str(var_arg))
        plt.xlabel('t')
        plt.legend()

def phaseDiagramOld(loc, varParamX, varParamY, plotObs, fixedIndicies= [], log=False, collapse=True, saveName='NOPLOT'):
    arg_ids, argsOfParamX, argsOfParamY, fixedArgStrings = prepare_2D_param_space_cut(loc, varParamX, varParamY, fixedIndicies)

    obss_v_arg = {}
    for obs in plotObs:
        v_arg = []
        for arg_idsVy in arg_ids:
            v_argVy=[]
            for arg_id in arg_idsVy:
                results = rdt.read_phaseDiagram(loc, arg_id)
                for row in results:
                    v_argVy.append(row[obs])
            v_arg.append(v_argVy)
        obss_v_arg[obs]=np.array(v_arg)

    XS, YS = np.meshgrid(argsOfParamX, argsOfParamY)
    if collapse:
        sweep_size = len(argsOfParamY)
        order = np.array(range(sweep_size))/float(sweep_size)
    for obs in plotObs:
        fig, ax = plt.subplots()
        plt.ylabel(varParamY)
        plt.xlabel(varParamX)
        plt.title(obs)
        if collapse:
            for yn, y in enumerate(argsOfParamY):
                if log:
                    plt.semilogx(argsOfParamX, obss_v_arg[obs][:,yn], label=str(y))
                else:
                    plt.plot(argsOfParamX, obss_v_arg[obs][:,yn], label=str(y))
                plt.legend()
        else:
            c = ax.pcolor(XS, YS, obss_v_arg[obs].T)
            plt.colorbar(c, ax=ax)
        if not (saveName == 'NOPLOT'):
            plt.savefig(saveName+obs+'.pdf')
        plt.show()

def phaseDiagram1D(loc, varParam, plotObs, fixedIndicies=[], plotFrom=0, plotEvery=1, var_indicies=[]):
    # read sweep data:
    swept_arg_ids, varParamValues, fixedArgString = prepare_param_space_1Dcut(loc, varParam, fixedIndicies, plotFrom, plotEvery, var_indicies)
    traces = {}
    results = []
    for arg_id, var_arg in zip(swept_arg_ids, varParamValues):
        rows = rdt.read_phaseDiagram(loc, arg_id)
        for row in rows:
            results.append(row)
    print(results)
    for obs in plotObs:
        yvalues = []
        for result in results:
            yvalues.append(result[obs])
        plt.plot(varParamValues, yvalues, label=obs)
    plt.legend()

