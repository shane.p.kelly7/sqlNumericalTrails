import sqlite3
import pyNumericalTrials.executionManager as em
def relTrialPath(localRowID, experimentName):
    return str(localRowID).zfill(2) + "_" + experimentName

def checkForMaster(dbFullName):
    conn = sqlite3.connect(dbFullName)
    cur  = conn.cursor()
    cur.execute("SELECT name FROM sqlite_master WHERE type='table' AND name='master'; ")
    masterExists = len(cur.fetchall())>0
    conn.commit()
    conn.close()
    return masterExists

def insertScriptTable(cur, experimentName, args, dependencies, globalID):
    """
    This function takes args from argParser, a script name, a set of dependencies keys and the globalID for the trial
    and it produces the entry in the "experimentName" table with dependencies and and parameters 
    
    dependencies is a list of strings (keys to the arg dictionary)

    it returns the "local" rowID and file location
    """
    insrtSQLhead = " Insert Into '" + experimentName + "'(masterID, computationTime, "
    insrtSQLtail = " Values (?, ?, "
    computationTime = -1
    insrtValues = [globalID, computationTime]
    createSQL = " Create Table If Not Exists '" + experimentName + "'(masterID INTEGER, computationTime DOUBLE"
    argsDict = args.__dict__

    if argsDict.__contains__('sweepfile'):
        sweepInfos = em.readSweepFile(args.sweepfile)
        argsSwept = [sweepInfo['argName'] for sweepInfo in sweepInfos]
    else:
        argsSwept = []
        

    # insert command does not keep track of order of keys, values in dictionary, thus second time it can insert randomly
    for key in argsDict:
        if key != 'sweepfile':
            keyType = type(argsDict[key])
            # TODO: if keytype list
            if key not in argsSwept:
                insrtValues.append(argsDict[key])
                insrtSQLtail +=" ?,"
                insrtSQLhead += key +","

            if key in dependencies:
                key = "d_"+key

            if keyType == str:
                createSQL+=" ,'"+key+"' TEXT"
            elif keyType == int:
                createSQL+=" ,'"+key+"' INTEGER"
            elif keyType == float:
                createSQL+=" ,'"+key+"' DOUBLE"
            else:
                createSQL+=" ,'"+key+"' TEXT"

    insrtSQLhead = insrtSQLhead[:-1] + ")"
    insrtSQLtail = insrtSQLtail[:-1] + ")"
    
    insrtSQL = insrtSQLhead + insrtSQLtail
    createSQL+=")"
    print(createSQL)
    print(insrtSQL)

    cur.execute(createSQL)
    cur.execute(insrtSQL, tuple(insrtValues))
    localRowID = cur.lastrowid

    # return rowID and file location
    return localRowID, relTrialPath(localRowID, experimentName)

def insertSweepFileTable(cur, experimentName, args, localID):
    tableName = experimentName+'sweeps'
    createSQL = " Create Table If Not Exists '" + tableName + "'(localID INTEGER, sweeptArg TEXT, linOlog TEXT, x0 DOUBLE, xf DOUBLE, N INT)"

    cur.execute(createSQL)

    sweepInfos = em.readSweepFile(args.sweepfile)
    for sweepInfoDic in sweepInfos:
        insrtSQLhead = " Insert Into '" + tableName + "'(localID, sweeptArg, linOlog, x0, xf, N)"
        insrtSQLtail = " Values (?,?,?,?,?,?) "
        insrtSQL = insrtSQLhead + insrtSQLtail

        linOlog   = sweepInfoDic['linOlog']  
        argName = sweepInfoDic['argName']
        x0        = sweepInfoDic['x0']       
        xf        = sweepInfoDic['xf']       
        n         = sweepInfoDic['n']        
        insrtValues = [localID, argName, linOlog, x0, xf, n]
        cur.execute(insrtSQL, tuple(insrtValues))

    viewName = tableName+'view'

    viewSQL = "Create View If Not Exists '" + viewName + "' as Select '" + tableName + "'.*, '" + experimentName + "'.* FROM '" + experimentName + "' inner join '" + tableName + "' on '" + experimentName + "'.rowid= '" + tableName + "' .localID"
    cur.execute(viewSQL)

def localIDfromMasterID(cur, masterID):
    # TODO: TEST this func
    # from master get experimentName
    cur.execute("SELECT experimentName FROM master where rowid = ?", (masterID,))
    experimentName = cur.fetchone()[0]
    cur.fetchall()

    # from experimentName Table get localID
    localQuery = """
        SELECT
            rowID
        FROM
            '{experimentName}'
        WHERE
            masterID = ?
    """.format(experimentName=experimentName)
    cur.execute(localQuery, (masterID,))
    localID = cur.fetchone()[0]
    cur.fetchall()
    return localID, experimentName

def localParamsFromMasterID(con, masterID):
    # TODO: TEST this func

    # experimentName form master ID
    con.row_factory = dict_factory
    cur = con.cursor()
    cur.execute("SELECT experimentName FROM master where rowid = ?", (masterID,))
    experimentName = cur.fetchone()["experimentName"]
    cur.fetchall()

    # params from experimentName and masterID
    localQuery = """SELECT * FROM '{experimentName}' where masterID = ?""".format(experimentName=experimentName)
    cur.execute(localQuery, (masterID,))

    paramDict = cur.fetchone()
    cur.close()
    return paramDict, experimentName


def dict_factory(cursor, row):
    # TODO: TEST this func
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

masterCreateCommand = """
    CREATE TABLE IF NOT EXISTS
        `master` (
            `timestamp` TEXT NOT NULL DEFAULT (datetime('now','localtime')),
            `description` TEXT,
            `experimentName` TEXT NOT NULL,
            `state` TEXT NOT NULL DEFAULT 'Initialized'
            )
    """
