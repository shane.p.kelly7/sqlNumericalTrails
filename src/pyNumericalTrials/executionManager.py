import numpy as np
import multiprocessing as mp
import itertools as it
import pyNumericalTrials.resultManagementTools as rmt
from shutil import copyfile
import signal
import sys

class executionManager(object):
    """ This class manages parellel execution for disorder averaging and sweeping a set of arguments.

    Args:
        args: args parsed by Args parse
        dataLocBase: path to where to store data (without ending '/') I'm sorry for having chosen this convension :(
        compute_point: function with format compute_point(args, swept_arg_names)->dictionary with keys given by result_keys
        result_keys: list giving names of results (optional if order of keys is important in saving a csv file)
        result_keys_format: dictionary with result_keys as the keys, and 0,1,2 and values giving dimention of array for that result.

    The arguments to be swept are given in a seperate file specified by
         args.sweepfile
    That file must have the format of a set of lines with entries:

        'linOlog', 'argName' , 'x0',  'xf', 'n' 
        str      , str       , float, float, int

    With this arg, this class generates the set of parameters to sweep over

    The function executeParalell() then uses the multiprocessing library
    to make calls to the function compute_point, this function should have the form:
        compute_point(args, dataDir)
    And it should (if you want to use averageNsave) then return:
        a dictionary with keys given by result_keys

    The function averageNsave will, if disorder was involved average over the disorder
    and then save the file to a csv called PhaseDiagram.csv (only if there are 0D results)  or an sqlDatabase

    In this way, a script that uses a single set of parameters acting on a single disorder realization can be turrned in the a multi-core script which averages over the disorder and can sweep any parameter.


    Attributes:
        args: args parsed by ArgParse
        dirBase: location where to save data (without ending '/') I'm sorry for having chosen this convension :(
        compute_point: function with format compute_point(args, swept_arg_names)->dictionary with keys given by result_keys
        result_keys: list giving names of results
        result_keys_format: dictionary with result_keys as the keys, and 0,1,2 and values giving dimention of array for that result.
        self.swept_arg_names, self.argValues2DListPseeds, self.argValues2DList: used for parameter sweep.
        swept_args_format: dictionary giving data type of parameter being swept.
        results: list of dictionaries with keys given by result_keys and values in format descirbed by result_keys_format

    """
    def __init__(self, args, dataLocBase, compute_point, results_format={}, result_keys=[]):
        """ Constructer for the executionManager object

        """
        self.args = args
        self.dirBase = dataLocBase
        self.compute_point = compute_point
        self.swept_arg_names, self.argValues2DListPseeds, self.argValues2DList = prep_argSweepCombinations(args)
        if len(result_keys)>0:
            self.result_keys = result_keys
        else:
            self.result_keys = list(results_format.keys())
        self.result_keys_format = results_format
        self.swept_args_format = {label_key:'float' for label_key in self.swept_arg_names}

    def executeParallel(self):
        """ executes code in parallel using map_async.
        """
        #exec and regroup Parallel Tasks
        pool = mp.Pool(self.args.CPUs)
        fixed = self.args, self.swept_arg_names, self.compute_point, self.dirBase
        localKern = partial(kernel, fixed)
        self.results = pool.map_async(localKern, self.argValues2DListPseeds).get()
        pool.close()
        pool.join()

    def averageNSave(self, sqlSave = True):
        """ Preforms disorder aveage and saves as csv or sql.

        sqlSave: bool whether to save in a sql file or a csv file.
        Currently, saving to a csv file can only save 0D dimention results.
        """
        # disorer average and save
        if self.args.nDisorders>1:
            averagedResults = averageSeedLabeledList(self.argValues2DList, self.results, self.swept_arg_names)
        else:
            averagedResults = stripSeedNcombine(self.results)

        if sqlSave:
            rmt.writeTabels(averagedResults, self.swept_args_format, self.result_keys_format, self.dirBase)
        else:
            csv_array_T, csv_keys = dicToCSV(averagedResults, self.swept_arg_names, self.result_keys)
            np.savetxt(self.dirBase+'/phaseDiagram.csv', csv_array_T.T, delimiter=',')
            copyfile(self.args.sweepfile, self.dirBase+'/sweepfile.csv')

#averaging tools
def dictionaryAvg(listOdics):
    """
    averages a list who elements are dictionary, the average is done key by key
    """
    keys = list(listOdics[0].keys())
    avgDic = {}
    for key in keys:
        avgDic[key]=avgKey(listOdics, key)
    return avgDic

def avgKey(listOdics, key):
    """
    takes a list and of dictionaries and returns of the average of values specified by a key
    """
    return np.average([dic[key] for dic in listOdics], axis=0)

# dictionary writing tools
def dicToCSV(listOdics, swept_arg_names, i_result_keys=[]):
    """
    takes a list of dictionaries with all the same keys and produces a 2D array, where the first index gives a key and the second gives an element in the list of dictionires:
    csv[key, dicIdx]
    """
    keys = list(listOdics[0].keys())
    for label_key in swept_arg_names:
        label_key_idx = keys.index(label_key)
        keys.pop(label_key_idx)
    csv = []
    # make sure label keys are the first indexes
    for key in swept_arg_names:
        csvrow = [dic[key] for dic in listOdics]
        csv.append(csvrow)
    if len(i_result_keys)>0:
        result_keys = i_result_keys
    else:
        result_keys = keys
    for key in result_keys:
        csvrow = [dic[key] for dic in listOdics]
        csv.append(csvrow)

    return np.array(csv), keys

# sweep tools
def stripSeedNcombine(results):
    """
    takes the results object: (a list of tuples of the form: (seed, sweep_args, results))
    removes the seed
    and returns a diction including keys for both the results and sweep_args
    """
    return [{**res[1], **res[2]} for res in results]
    # return [res[2] for res in results]

def averageSeedLabeledList(argValues2DList, results, swept_arg_names):
    """
    Similar to stripSeedNcombine, but with a disorder average involved
    """
    averagedResults = []
    for swept_arg_values in argValues2DList:
        swept_args = {}
        # construct label dictionary
        for arg_name, arg_value in zip(swept_arg_names, swept_arg_values):
            swept_args[arg_name]=arg_value
        # take from results elements which only have all the same params and construct a list of
        #   params+results dictionarys
        disorderResults = [{**res[1], **res[2]} for res in results if res[1]==swept_args]

        # average those dictionaries over the list index
        avg4param = dictionaryAvg(disorderResults)

        # these new averaged dictionaries form the averagedResults to be saved to file
        averagedResults.append(avg4param)
    return averagedResults

def prep_argSweepCombinations(args):
    """ 
    uses sweep file to generate keys(arg parameters to sweep) and their values
    This function uses args.sweepfile to 
     1) generate a list of args that are sweept: ['arg1','arg2',...]: swept_arg_names
     2) generate table of all the different argument combinations: [[arg1,arg2,...]]: argValues2DList
     3) if disorder average involve, append a seed varible to that table: [[arg1,arg2,...,seed]]: argValues2DListPseeds

    The function will also split the argValueTable 
    """
    paramSweepDescriptions = readSweepFile(args.sweepfile)
    swept_arg_names = []
    argsToSweep=[]
    # first generate a list of possible values for each arguement individually
    for sweepInfoDic in paramSweepDescriptions:
        linOlog   = sweepInfoDic['linOlog']  
        paramName = sweepInfoDic['argName']
        x0        = sweepInfoDic['x0']       
        xf        = sweepInfoDic['xf']       
        n         = sweepInfoDic['n']        
        if linOlog=='log':
            param = np.logspace(x0,xf, n)
        else:
            param = np.linspace(x0,xf, n)
        argsToSweep.append(param)
        swept_arg_names.append(paramName)
    # product those lists together to get the table of all different arguement value combinatins:
    argValues2DList = list(it.product(*argsToSweep))

    # check if disorder and add seeds
    if args.nDisorders > 1:
        seeds = np.array(range(args.nDisorders))
        argsToSweep.append(seeds)
    # take all possible combinations of the parameters and disorder keys
    argValue2DListPlusSeeds= list(it.product(*argsToSweep))

    return swept_arg_names, argValue2DListPlusSeeds, argValues2DList

def tupleFlatten(mixed):
    return tuple(it.chain(*(i if isinstance(i, tuple) else (i,) for i in mixed)))

def readSweepFile(sweepfile):
    """
    sweepfile is a file of lines. Each line describes how to sweep a parameter:
    linOlog, argName, x0, xf, n
    i.e:
    lin, example,   0, 5.32, 100
    log, example2, -1, 1,    100
    This function reads that file and produces a list of dictionaries for each line
    """
    sweepInfos = np.genfromtxt(sweepfile, delimiter=",", dtype=None, autostrip=True, encoding='utf-8')
    if len(sweepInfos.shape)==0:
        sweepInfos = np.array([sweepInfos])
    sweepInfoList =  []
    for sweepInfo in sweepInfos:
        print(sweepInfo)
        sweepInfoDic = {}
        sweepInfoDic['linOlog']   = sweepInfo[0]
        sweepInfoDic['argName']   = sweepInfo[1]
        sweepInfoDic['x0']        = float(sweepInfo[2])
        sweepInfoDic['xf']        = float(sweepInfo[3])
        sweepInfoDic['n']         = int(sweepInfo[4])
        sweepInfoList.append(sweepInfoDic)
    return sweepInfoList

from functools import partial


def kernel(fixed, sweep_arg_values):
    """
    this is the kernel for each CPU core, it takes a set of fixed parameters of the form:
    --fixed = (args, sweet_arg_names, compute_point, original_sigint_handler)
        --currently: original_sigint_handler is ignored
        --compute_point: is some function which takes args and swept_arg_names

        --sweep_arg_values: is different for each core, they are a list of values for th keys in swept_arg_names
           both should be lists of same length with 1-1 maping of key->values

    this function returns a tuple: (seed, sweep_args, results)
     --sweep_args: is a dictionary which identifies the arugments that were swept and there values corresponding to this result
     --results: is a dictionary with keys given by result_keys
    """
    # if there is disorder averaging to be done, pop the seed
    args, sweep_arg_names, compute_point, datadir = fixed
    if args.nDisorders>1:
        seed = sweep_arg_values[-1]
        np.random.seed(seed)
        non_seed_arg_values = sweep_arg_values[:-1]
    else:
        seed = 0
        non_seed_arg_values = sweep_arg_values

    # overwrite the base args with the sweep parameters
    sweep_args = {}
    for sweep_arg_name, sweep_arg_value in zip(sweep_arg_names, non_seed_arg_values):
        if type(args.__dict__[sweep_arg_name])==int:
            args.__dict__[sweep_arg_name]=int(sweep_arg_value)
        else:
            args.__dict__[sweep_arg_name]=sweep_arg_value
        sweep_args[sweep_arg_name]=sweep_arg_value

    #### The big work!:--------
    results = compute_point(args, datadir)
    #### ----------------------

    # result is constructed so disorder average can extract params and results based on
    # a params hash
    return (seed, sweep_args, results)

