# in master table, need script name and description, date and id generated aautomatically
# in script table, can make id and get master id just fine

# -- the script table must use an argParse to generatoe {params}
# -- there then needs to be two more functions:
#       -- identify dependencies: returns a dictionary with depency label and ids
#       -- same for outputs
#     the relevant info for a dependency is the location of it's data
#     thus how we describe output data is critical to both these functions
#     if we standarize the file structure we can abstract out the general parts of this:
#
#  Trials/
#    trials.db
#    00_scriptAName/
#       trialId_trialName/
#         data.csv
#         data.db
#         subfolders/
#           ...
#    01_scriptAName/
#           ...
#    00_scriptBName/
#           ...
#
#    output info will be path relative to Trials/
#    dependcy function will return absolute path to trialId_trialName/
#    specific code info will manage from there
#
#    TODO: the script.py should be called an       expirmenet
#          the particular call of that script is a trial
#           localID -> expirementID
#           globalID -> masterID
#            
#           maybe instead of master, it can be comptuer.
#           This way I can be clear about what computer I'm running something on
import sqlite3
import os
import pyNumericalTrials.sqlTools as sqlt
import time




class trialManager(object):
    """ This class creates an object which keeps track of the trials preformed by the experiment script it is called in.

    Args:
        argParser: ArgParser parser that defines parameters for the experiment.
        experimentName: String giving name of the experiment table.
        scriptDescription: A string.
        dbLoc: Path to directory where the experiments database is located.
        dbName: Name of experiments database default: experiments.db
        addMaster: default=True if the masterTable or database does not exists, it is created when this function is called.
        dependencies: Not used right now.

    When this object is created, it only makes/loads the database.
    When :func:`parseNadd()<pyNumericalTrials.trialManager.trialManager.parseNadd>` is called a new trial with the parsed arguments is added to the scripts experiment table.
    When :func:`finalize()<pyNumericalTrials.trialManager.trialManager.finalize>` is called the database is updated marking the time it took to compleate the computation.

    Attributes:
        experimentName: String giving name of the experiment table.
        parser: ArgParse parser which defines the parameters for the experiment.
        dbFullName: Database file name and location.
        trialLoc: Location trial data should be stored at.

    """
    def __init__(self, argParser, experimentName, scriptDescription, dbLoc, dbName = 'experiments.db', dependencies=[], addMaster =True):
        # check that data base exists
        self.dbFullName = dbLoc + dbName
        print(dbLoc)
        self.trialLoc = dbLoc

        if not sqlt.checkForMaster(self.dbFullName):
            if addMaster:
                conn = sqlite3.connect(self.dbFullName)
                cur  = conn.cursor()
                cur.execute(sqlt.masterCreateCommand)
                conn.commit()
                conn.close()
                print("Master added")
            else:
                raise Exception("master not found in database")
        # TODO: this should have the option to make master if it doesn't exists

        # prepare sqliteCommands
        self.experimentName = experimentName
        self.scriptDescription = scriptDescription
        self.parser = argParser
        self.parser.add_argument('--description',default='NOT GIVEN')
        self.rowID=-1
        self.dependencies=dependencies

    def parseNadd(self):
        """ Parses the arguements in parser and adds arguments for the trial to the experiment table.

        This function also starts a timer to track how long the code takes.

        Returns: (dataLoc, args)
           * dataLoc: Location created to store data produced by trial.
           * args:    args parsed by argParser.
        """
        # read argParse
        self.args = self.parser.parse_args()

        #openDB
        conn = sqlite3.connect(self.dbFullName)
        cur  = conn.cursor()

        #create master entry
        cur.execute("Insert into master (experimentName, description) Values (?,?)", (self.experimentName, self.args.description))
        globalID = cur.lastrowid
        self.rowID = globalID

        # process dependencies and args into table
        localID, folderNamePrefix = sqlt.insertScriptTable(cur, self.experimentName, self.args, self.dependencies, globalID)
        self.localID = localID

        # added to sweepfile table
        if self.args.__dict__.__contains__('sweepfile'):
            sqlt.insertSweepFileTable(cur, self.experimentName, self.args, localID)

        conn.commit()
        conn.close()

        # set and make location folder
        folderName = folderNamePrefix
        # if first trial for a specific script, make script (expirment) foulder
        if not os.path.exists(self.trialLoc+self.experimentName):
            os.mkdir(self.trialLoc+self.experimentName)
        self.outputFolder = self.experimentName +"/"+folderName + "/"
        if not os.path.exists(self.trialLoc+self.outputFolder):
            os.mkdir(self.trialLoc+self.outputFolder)
        

        # return locationFolder
        # TODO: possibly return trial object instead of this 
        self.startTime = time.perf_counter()
        return (self.trialLoc+self.outputFolder, self.args)

    def getDependenciesFileLocs(self):
        """
        Untested
        """
        # TODO: TEST this func
        conn = sqlite3.connect(self.dbFullName)
        cur  = conn.cursor()
        depLocs = {}
        # loop through dependencies
        for dep in self.dependencies:
            # get dependency master id
            depMasterID = self.args.__dict__[dep]

            # get script name and local ID
            localID, experimentName = sqlt.localIDfromMasterID(cur, depMasterID)

            # generate location string
            depLocs[dep] = self.trialLoc + experimentName + '/' + sqlt.relTrialPath(localID, experimentName)
        conn.close()
        return depLocs

    def getDependenciesParameters(self):
        """
        Untested
        """
        # TODO: TEST this func
        conn = sqlite3.connect(self.dbFullName)
        depParams = {}
        # loop through dependencies
        for dep in self.dependencies:
            # get dependency master id
            depMasterID = self.args.__dict__[dep]
            depParams[dep] = sqlt.localParamsFromMasterID(conn, depMasterID)
        return depParams

    def finalize(self):
        """ Adds execution time and marks trial as completed.
        """
        self.endTime = time.perf_counter()
        total_time = self.endTime-self.startTime
        finalizeCmd = " Update master Set state = 'Valid' where rowid = ?"
        updateTimeCmd = "Update '" + self.experimentName + "' Set computationTime = ? where rowid = ?"
        conn = sqlite3.connect(self.dbFullName)
        cur  = conn.cursor()
        if self.rowID >0:
            cur.execute(finalizeCmd, (self.rowID,))
            cur.execute(updateTimeCmd, (total_time, self.localID))
        conn.commit()
        conn.close()


def readTrial(dbLoc, dbName, masterID):
    """
    Args:
        dbLoc: path where experiments database is
        dbName: name of experiments database

    Returns: (trialParams, dataLoc)
        trialParams: dictionary of the trial parameters requested
        dataLoc: location of the data for the trial requested.
    """
    dbFullName = dbLoc + dbName
    print(dbFullName)
    conn = sqlite3.connect(dbFullName)
    trialParams = sqlt.localParamsFromMasterID(conn, masterID)
    print(trialParams)
    conn.close()
    conn = sqlite3.connect(dbFullName)
    cur  = conn.cursor()
    localID, experimentName = sqlt.localIDfromMasterID(cur, masterID)
    dataLoc = dbLoc + experimentName + '/' + sqlt.relTrialPath(localID, experimentName)
    # TODO: return trial object
    return trialParams, dataLoc

# TODO:
class Trial(object):
    def __init__(self, masterID, dbLoc, dbName = 'trials.db'):
        pass


def insertScriptTableFunc():
    print("Not Implemented")
    # TODO: this will be a decorator that can be used inside a trial script to create sub trials
    #https://docs.python.org/3/tutorial/controlflow.html#unpacking-argument-lists
