import argparse
import pyNumericalTrials.trialManager as tm

parser = argparse.ArgumentParser(description='An example expeirment script')
parser.add_argument('--exampleParam', type=int, default=3 ,help='description of parameter')
parser.add_argument('--anotherExample', type=float, default=1 ,help='description of parameter')

scriptName = "01_example"
databaseDir = "./"
trialInitser = tm.trialManager(parser, scriptName, "discription of trial", databaseDir, addMaster = True)

if __name__ == '__main__':
    args = trialInitser.parseNadd()
    print(args)
    trialInitser.finalize()
