"""
Can be used as a template for:
 * Tracking Parameters
 * Sweeping parameters: see sweepfile.csv
 * Saving 0,1 and 2D data
 * Disorder averaging: 

If your orginal script used args.parse(), don't forget to remove that in compute_point.
If you keep it there it will ignore the paramters set by sweeping.
"""
import pyNumericalTrials.trialManager as tm
import pyNumericalTrials.executionManager as em

# CORE
import numpy as np
import argparse

# import dirs

parser = argparse.ArgumentParser(description='Description for arugments')
# necessary new paramters to use this template
parser.add_argument('--sweepfile', default='sweepfile.csv', help = 'file describing which parameters to sweep')
parser.add_argument('--nDisorders', type = int, default=100, help = 'Number of random samples. Set to one to avoid disorder averaging')
parser.add_argument('--CPUs', type = int, default=4, help = 'number of CPUs to USE')

# example parameters of base script
parser.add_argument('--example1', type = float, default=1.0, help = 'example paramters')
parser.add_argument('--example2', type = float, default=2.0, help = 'example paramters')
parser.add_argument('--example3', type =   int, default=2, help = 'example paramters')

# used for trial initialization
scriptName   = "sweepNsave"
databaseDir  = "./Data/"
trialInitser = tm.trialManager(parser, scriptName, "discription", databaseDir, addMaster = True)

# specifies structure of result dictonary
results_format = {'mean': 0, 'var': 0, 'trace': 1, 'twoDdata': 2} # integer is dimention of data: currently only to 2D

def compute_point(args, label_keys):
    # do computation
    x = np.random.rand(1)[0]*args.example3
    # return "results dictionary"
    return {'mean':x,
            'var':x*x,
            'trace': np.linspace(0,10*x,5),
            'twoDdata': np.outer(range(10),range(10))
           }

if __name__ == '__main__':
    dataLoc, args = trialInitser.parseNadd()

    dnsCTRL = em.executionManager(args, dataLoc, compute_point, results_format)
    print("Executing...")
    dnsCTRL.executeParallel()
    print("Saving...")
    dnsCTRL.averageNSave(sqlSave = True)

    trialInitser.finalize()
