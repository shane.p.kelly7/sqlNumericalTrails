import pyNumericalTrials.readNplot as rnp

if __name__ == '__main__':
    result_keys = ['mean','var','trace']
    result_keys_format = {'mean': 0, 'var': 0, 'trace': 1}
    paramList, resultDictList = rnp.get_results('./Data/sweepNsave/01_sweepNsave/', result_keys_format)
    print(rnp.get_results('./Data/sweepNsave/01_sweepNsave/', result_keys_format))
