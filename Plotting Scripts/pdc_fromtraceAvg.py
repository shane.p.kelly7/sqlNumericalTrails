import argparse
import numpy as np
import resultDataTools as rdt
from matplotlib import pyplot as plt

parser = argparse.ArgumentParser(description='Description for arugments')
# necessary new paramters to use this template
parser.add_argument('--loc', help = 'discription')
parser.add_argument('--varArg', default='')
# parser.add_argument('--fixArgs', default=[''], nargs='+')
parser.add_argument('--fixedIndicies', default=[], type=int, nargs='+', help= 'fixed indicies for each varied param, indicies should be given in order of the columns in the data base, a 0 indicies should also be given for the varArg')
parser.add_argument('--plotObs', default=['trace'],  nargs='+')
parser.add_argument('--aa', default=0.5, type=float, help= 'fractionToAverageAfter')
parser.add_argument('--at', default=0.8, type=float, help= 'fractionToAverageAfter')


if __name__ == '__main__':
    args = parser.parse_args()
    print(args.loc)
    sweptParamArray, uniqueParams, spdict, column_names = rdt.read_sweptArguments(args.loc)
    # column_names.pop(column_names.index(args.varArg))

    # check if fixed indicies are given
    if len(args.fixedIndicies)==0:
        fixedIndicies = np.int_(np.zeros(len(column_names)))
    else:
        fixedIndicies = args.fixedIndicies

    param_row_ids = []
    if args.varArg == '':
        paramsInSweep =[0]
    else:
        paramsInSweep = np.sort(uniqueParams[args.varArg])
    print(paramsInSweep)
    # go through all the args in the sweep arg
    for varParam in paramsInSweep:
        uniqueParam = []
        # generate the unique tupple to identify the index given the args specified by the fixed indicies
        for n, col in enumerate(column_names):
            if col == args.varArg:
                uniqueParam.append(varParam)
            else:
                uniqueParam.append(uniqueParams[col][fixedIndicies[n]])
        arg_id =spdict[tuple(uniqueParam)]
        param_row_ids.append(arg_id)

    obss_v_arg = {}
    print(paramsInSweep)
    print(args.plotObs)
    for obs in args.plotObs:
        avgs = []
        for arg_id, paramInSweep in zip(param_row_ids, paramsInSweep):
            traceBobservables =  rdt.read_trace(args.loc, arg_id)
            trace = [ row[obs] for row in traceBobservables]
            averageFrom = int(len(trace)*args.aa)
            averageTo = int(len(trace)*args.at)
            avgs.append(np.average(trace[averageFrom:averageTo]))
        plt.plot(paramsInSweep, avgs, label=obs)
    plt.title(args.varArg + ': ' + str(paramInSweep))
    plt.xlabel(args.varArg)
    plt.legend()
    plt.show()
