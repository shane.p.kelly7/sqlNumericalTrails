import argparse
import numpy as np
import resultDataTools as rdt
from matplotlib import pyplot as plt

parser = argparse.ArgumentParser(description='Description for arugments')
# necessary new paramters to use this template
parser.add_argument('--loc', help = 'discription')
parser.add_argument('--varArg', default='')
# parser.add_argument('--fixArgs', default=[''], nargs='+')
parser.add_argument('--fixedIndicies', default=[], type=int, nargs='+', help= 'fixed indicies for each varied param, indicies should be given in order of the columns in the data base, a 0 indicies should also be given for the varArg')
parser.add_argument('--plotObs', default=['trace'],  nargs='+')
parser.add_argument('--log', action='store_true')
parser.add_argument('--logy', action='store_true')
parser.add_argument('--loglog', action='store_true')
parser.add_argument('--dt', default=-1, type=float,  help= '')
parser.add_argument('--plotFrom', default=0, type=int, help= 'restrict varArg values')
parser.add_argument('--plotEvery', default=1, type=int, help= 'restrict varArg values')

import traceLegendParam as tlp

if __name__ == '__main__':
    args = parser.parse_args()
    ##### PREPRARE SWEPT DATA #######
    swept_arg_ids, varArgValues, fixedArgString = tlp.prepare_swept_data(args)

    for arg_id, paramInSweep in zip(swept_arg_ids, varArgValues):
        for obs in args.plotObs:
            traceBobservables =  rdt.read_trace1D(args.loc, arg_id)
            trace = [ row[obs] for row in traceBobservables]
            t = np.array(range(len(trace)))
            if args.dt>0:
                t=t*args.dt
            if args.log:
                plt.semilogx(t, trace, label = obs)
            elif args.loglog:
                plt.loglog(t, trace, label = obs)
            elif args.logy:
                plt.semilogy(t, trace, label = obs)
            else:
                plt.plot(t, trace, label = obs)
        plt.title(args.varArg + ': ' + str(paramInSweep))
        plt.xlabel('n_step')
        plt.legend()
        plt.show()
