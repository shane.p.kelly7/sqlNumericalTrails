import argparse
import numpy as np
import resultDataTools as rdt
from matplotlib import pyplot as plt

parser = argparse.ArgumentParser(description='Description for arugments')
# necessary new paramters to use this template
parser.add_argument('--loc', help = 'discription')
parser.add_argument('--varArg', default='example3')
# parser.add_argument('--fixArgs', default=[''], nargs='+')
parser.add_argument('--fixedIndicies', default=[], type=int, nargs='+', help= 'fixed indicies for each varied param, indicies should be given in order of the columns in the data base, a 0 indicies should also be given for the varArg')
parser.add_argument('--plotObs', default=['mean'],  nargs='+')
parser.add_argument('--log', action='store_true')

if __name__ == '__main__':
    args = parser.parse_args()
    print(args.loc)
    sweptParamArray, uniqueParams, spdict, column_names = rdt.read_sweptArguments(args.loc)
    # column_names.pop(column_names.index(args.varArg))

    # check if fixed indicies are given
    if len(args.fixedIndicies)==0:
        fixedIndicies = np.int_(np.zeros(len(column_names)))
    else:
        fixedIndicies = args.fixedIndicies

    param_row_ids = []
    paramsInSweep = np.sort(uniqueParams[args.varArg])
    print(paramsInSweep)
    # go through all the args in the sweep arg
    for varParam in paramsInSweep:
        uniqueParam = []
        # generate the unique tupple to identify the index given the args specified by the fixed indicies
        for n, col in enumerate(column_names):
            if col == args.varArg:
                uniqueParam.append(varParam)
            else:
                uniqueParam.append(np.sort(uniqueParams[col])[fixedIndicies[n]])
                print(np.sort(uniqueParams[col])[fixedIndicies[n]])
        arg_id =spdict[tuple(uniqueParam)]
        param_row_ids.append(arg_id)

    obss_v_arg = {}
    print(paramsInSweep)
    print(param_row_ids)
    for obs in args.plotObs:
        v_arg = []
        for arg_id in param_row_ids:
            results = rdt.read_Phase_phaseDiagram_row(args.loc, arg_id)
            for row in results:
                # print(row)
                v_arg.append(row[obs])
        obss_v_arg[obs]=v_arg
        if args.log:
            plt.semilogx(paramsInSweep, v_arg, label = obs)
        else:
            plt.plot(paramsInSweep, v_arg, label = obs)
    plt.legend()
    plt.xlabel(args.varArg)
    plt.show()
