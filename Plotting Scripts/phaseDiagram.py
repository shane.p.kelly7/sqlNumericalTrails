import argparse
import numpy as np
import resultDataTools as rdt
from matplotlib import pyplot as plt
from cycler import cycler

parser = argparse.ArgumentParser(description='Description for arugments')
# necessary new paramters to use this template
parser.add_argument('--loc', help = 'discription')
parser.add_argument('--varArgX', default='example2')
parser.add_argument('--varArgY', default='example3')
# parser.add_argument('--fixArgs', default=[''], nargs='+')
parser.add_argument('--fixedIndicies', default=[], type=int, nargs='+', help= 'fixed indicies for each varied param, indicies should be given in order of the columns in the data base, a 0 indicies should also be given for the varArg')
parser.add_argument('--plotObs', default=['mean'],  nargs='+')
parser.add_argument('--log', action='store_true')
parser.add_argument('--collapse', action='store_true')
parser.add_argument('--saveName', default='NOPLOT')

if __name__ == '__main__':
    args = parser.parse_args()
    print(args.loc)
    sweptParamArray, uniqueParams, spdict, column_names = rdt.read_sweptArguments(args.loc)
    print(sweptParamArray)
    print(uniqueParams)
    print(spdict)
    print(column_names)

    # check if fixed indicies are given
    if len(args.fixedIndicies)==0:
        fixedIndicies = np.int_(np.zeros(len(column_names)))
    else:
        fixedIndicies = args.fixedIndicies

    param_row_ids = []
    paramsInSweepX = np.sort(uniqueParams[args.varArgX])
    paramsInSweepY = np.sort(uniqueParams[args.varArgY])
    # go through all the args in the sweep arg
    for varParamX in paramsInSweepX:
        param_row_idsVy = []
        for varParamY in paramsInSweepY:
            uniqueParam = []
            # generate the unique tupple to identify the index given the args specified by the fixed indicies
            for n, col in enumerate(column_names):
                if col == args.varArgX:
                    uniqueParam.append(varParamX)
                elif col == args.varArgY:
                    uniqueParam.append(varParamY)
                else:
                    uniqueParam.append(uniqueParams[col][fixedIndicies[n]])
            arg_id =spdict[tuple(uniqueParam)]
            param_row_idsVy.append(arg_id)
        param_row_ids.append(param_row_idsVy)

    obss_v_arg = {}
    for obs in args.plotObs:
        v_arg = []
        for arg_idsVy in param_row_ids:
            v_argVy=[]
            for arg_id in arg_idsVy:
                results = rdt.read_Phase_phaseDiagram_row(args.loc, arg_id)
                for row in results:
                    v_argVy.append(row[obs])
            v_arg.append(v_argVy)
        obss_v_arg[obs]=np.array(v_arg)
    # plt.plot(paramsInSweep, v_arg, label = obs)
    XS, YS = np.meshgrid(paramsInSweepX, paramsInSweepY)
    if args.collapse:
        sweep_size = len(paramsInSweepY)
        order = np.array(range(sweep_size))/float(sweep_size)
        plt.rc('axes', prop_cycle=(cycler(color=[[0, np.sqrt(1-x), np.sqrt(x)] for x in order])))
    for obs in args.plotObs:
        fig, ax = plt.subplots()
        plt.ylabel(args.varArgY)
        plt.xlabel(args.varArgX)
        plt.title(obs)
        # print(XS)
        # print(YS)
        # print(np.array(obss_v_arg[obs]))
        print(paramsInSweepX)
        if args.collapse:
            # obss_v_arg = np.array(obss_v_arg)
            for yn, y in enumerate(paramsInSweepY):
                if args.log:
                    plt.semilogx(paramsInSweepX, obss_v_arg[obs][:,yn], label=str(y))
                else:
                    plt.plot(paramsInSweepX, obss_v_arg[obs][:,yn], label=str(y))
                # print(obss_v_arg[obs][:,yn])
                plt.legend()
        else:
            c = ax.pcolor(XS, YS, obss_v_arg[obs].T)
            plt.colorbar(c, ax=ax)
        if not (args.saveName == 'NOPLOT'):
            plt.savefig(args.saveName+obs+'.pdf')
        plt.show()

