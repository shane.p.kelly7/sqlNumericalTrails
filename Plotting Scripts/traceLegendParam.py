import argparse
import numpy as np
import resultDataTools as rdt
from matplotlib import pyplot as plt

parser = argparse.ArgumentParser(description='This script creates a plot of traces for a given trial where some arguements were swept')
# necessary new paramters to use this template
parser.add_argument('--loc', help = 'location of results.db')
parser.add_argument('--varArg', default='variable to be varied in legend')
parser.add_argument('--fixedIndicies', default=[], type=int, nargs='+', help= 'fixed indicies for each varied param, indicies should be given in order of the columns in the data base, a 0 indicies should also be given for the varArg')
parser.add_argument('--plotObs', default=['trace'],  nargs='+', help='list of observables to be plotted')
parser.add_argument('--log', action='store_true')
parser.add_argument('--logy', action='store_true')
parser.add_argument('--loglog', action='store_true')
parser.add_argument('--fit', action='store_true')
parser.add_argument('--dt', default=-1, type=float, help= 'if trace has secrete x value is proportional to the integers, this is the scale factor')
parser.add_argument('--plotFrom', default=0, type=int, help= 'restrict varArg values')
parser.add_argument('--plotEvery', default=1, type=int, help= 'restrict varArg values')
parser.add_argument('--saveName', default='NOPLOT')
parser.add_argument('--title', default='')
parser.add_argument('--xlabel', default='t')

def getSubTrialIdsFor1Sweep(varArgValues, fixedIndicies, uniqueArgValues, sweptArgNames, ToArgIdDict, args):
    """
    gets arg_ids for data to be read.
    requires:
    varArgValues:  values taken by the arguement being varied in the legend
    fixedIndicies: indicies of fixed arguemtns
    uniqueArgValues: unique values taken by different argumetns in the sweep 
    sweptArgNames
    ToArgIdDict
    """
    swept_arg_ids = []
    for varArgValue in varArgValues:
        # generate the unique tupple to identify the index given the args specified by the fixed indicies
        argIdDict_key = []
        for n, col in enumerate(sweptArgNames):
            if col == args.varArg:
                argIdDict_key.append(varArgValue)
            else:
                argIdDict_key.append(uniqueArgValues[col][fixedIndicies[n]])
        arg_id =ToArgIdDict[tuple(argIdDict_key)]
        swept_arg_ids.append(arg_id)
    return swept_arg_ids

def prepare_swept_data(args):
    """
    returns:
    swept_arg_ids: arg_ids for reading trace table
    varArgValues:  values taken by the arguement being varied in the legend
    fixedArgString: string describing the value of the other arguements
    """

    # read sweptArgument table
    sweptArgArray, uniqueArgValues, ToArgIdDict, sweptArgNames = rdt.read_sweptArguments(args.loc)

    # check if fixed indicies are given
    if len(args.fixedIndicies)==0:
        fixedIndicies = np.int_(np.zeros(len(sweptArgNames)))
    else:
        fixedIndicies = args.fixedIndicies

    # get values taken by the varArg
    if args.varArg == '':
        raise('need varArg')
    else:
        varArgValues = np.sort(uniqueArgValues[args.varArg])
    varArgValues = varArgValues[args.plotFrom::args.plotEvery]

    # string describing values kept constant in sweep
    fixedArgString = ""
    for n, col in enumerate(sweptArgNames):
        if not col == args.varArg:
            fixedArgString += str(col) + '='
            fixedArgString += str(uniqueArgValues[col][fixedIndicies[n]])
            fixedArgString += '|'

    swept_arg_ids = getSubTrialIdsFor1Sweep(varArgValues, fixedIndicies, uniqueArgValues, sweptArgNames, ToArgIdDict, args)
    return swept_arg_ids, varArgValues, fixedArgString

if __name__ == '__main__':
    args = parser.parse_args()
    ##### PREPRARE SWEPT DATA #######
    swept_arg_ids, varArgValues, fixedArgString = prepare_swept_data(args)

    ###### Plotting #######
    from cycler import cycler
    # prepare colors
    sweep_size = len(varArgValues)
    if sweep_size>8:
        order = np.array(range(sweep_size))/float(sweep_size)
        plt.rc('axes', prop_cycle=(cycler(color=[[0, np.sqrt(1-x), np.sqrt(x)] for x in order])))
    print(args.plotObs)

    # different plots for different observables
    for obs in args.plotObs:
        n=0
        for arg_id, paramInSweep in zip(swept_arg_ids, varArgValues):
            # read 1D trace from database
            subTrial1DtraceTable =  rdt.read_trace1D(args.loc, arg_id)
            trace = rdt.extract1DtraceForObs(subTrial1DtraceTable, obs)  

            # prepare x values
            t = np.array(range(len(trace)))
            if args.dt>0:
                t=t*args.dt
            x=np.array(range(len(trace)))

            # prepare legend label
            if sweep_size<8:
                labelname = paramInSweep
            elif n==0 or n==sweep_size-1:
                labelname = args.varArg + ': ' + str(np.round(paramInSweep,3))
            else:
                labelname = ''

            # plot given a scale
            if args.log:
                plt.semilogx(t, trace, label = labelname)
            elif args.logy:
                plt.semilogy(t, trace, label = labelname)
            elif args.loglog:
                plt.loglog(t, trace, label = labelname)
            else:
                plt.plot(t, trace, label = labelname)

            # try fitting a straght line
            if args.fit:
                if args.log:
                    print("fit", np.polyfit(np.log(x[1000:]), trace[1000:],1))
                else:
                    print("fit", np.polyfit((x[1000:]), trace[1000:],1))
                n+=1
        if args.title =='':
            plt.title(fixedArgString)
        else:
            plt.title(args.title)
        plt.ylabel(obs)
        plt.xlabel(args.xlabel)
        plt.legend()
        if not (args.saveName == 'NOPLOT'):
            plt.savefig(args.saveName+obs+'.pdf')
        plt.show()
